package HW1.com;


import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class RandomNumber {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        Random random = new Random();
        int randNum = random.nextInt(6);
        String [] [] events= new String[6][2];
        events[0][0]= "When did the World War II begin?";
        events[0][1]= "1939";
        events[1][0]= "When did the World War II finish?";
        events[1][1]= "1945";
        events[2][0]= "When did the World War I begin?";
        events[2][1]= "1914";
        events[3][0]= "When did the World War I finish?";
        events[3][1]= "1918";
        events[4][0]= "When did the Iraq war begin?";
        events[4][1]= "2003";
        events[5][0]= "When did the Iraq war finish?";
        events[5][1]= "2011";
        System.out.println("Enter your name: ");
        String name = input.next();

        System.out.println("Let the game begin!");
        System.out.println(events[randNum][0]);

        Integer num;
        while (!input.hasNextInt()) {
            System.out.println("That is not a number! Enter your number: ");
            input.next();
        }
        num = input.nextInt();

        Integer [] numbers= {num};
        int answer = Integer.parseInt(events[randNum][1]);
        while (answer != num) {

            if (num > answer) {
               System.out.println("Your number is too big. Please, try again.");
                while (!input.hasNextInt()) {
                    System.out.println("That not a number! Enter your number: ");
                    input.next();
                }
               num= input.nextInt();

            } else {
                System.out.println("Your number is too small. Please, try again.");
                while (!input.hasNextInt()) {
                    System.out.println("That is not a number! Enter your number: ");
                    input.next();
                }
                num= input.nextInt();
            }

            Integer [] tempNumbers = Arrays.copyOf(numbers,numbers.length+1);
            tempNumbers[numbers.length]= num;
            numbers=tempNumbers;
        }

        for (int i = numbers.length-1; i>0; i--){
            for(int j = 0; j<i; j++){
                if (numbers[j]<numbers[j+1]){
                    int tmp = numbers[j];
                    numbers[j]=numbers[j+1];
                    numbers[j+1]= tmp;
                }
            }
        }
        System.out.println("Your numbers: "+ Arrays.toString(numbers));
        System.out.println("Congratulations, " + name + "!");
    }
}

